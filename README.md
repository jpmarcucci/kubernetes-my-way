# Kubernetes my way :)

# Video fazendo o lab:
https://www.youtube.com/watch?v=hl4xHM5Q72Q

# Configurando o VirtualBox

Vamos criar 5 VMs no VirtualBox e para facilitar vamos usar os seguintes IPs e respectivos hostnames:  

192.168.56.150  kubemaster  
192.168.56.151  etcd1  
192.168.56.152  etcd2  
192.168.56.153  worker1  
192.168.56.154  worker2  

Crie uma rede virtual no VirtualBox com as seguintes especificacoes:  

Host NetworkManager - vboxnet0 - 192.168.56.1/24  

Configure todas as VMs com duas placas de rede, sendo a primeira NAT e a segunda host-only adapter (vboxnet0) com os ips 192.168.56.1/24  
Adaptador 1 - NAT  
Adaptador 2 - host-only adapter (vboxnet0)  

Para facilitar desative o selinux das maquinas:  
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config  
setenforce 0  

Vamos desabilitar tambem o firewall:  
systemctl disable firewalld && systemctl stop firewalld  

Em todos as VMs adicione as linhas ao /etc/sysctl.conf (Necessario para rodar o calico e encaminhamento de pacotes):  
net.ipv4.ip_forward=1  
net.ipv4.conf.all.rp_filter=0  

sysctl -p


Coloque os IPs no arquivo /etc/hosts de todos os servidores:  
192.168.56.150  kubemaster  
192.168.56.151  etcd1  
192.168.56.152  etcd2  
192.168.56.153  worker1  
192.168.56.154  worker2  

# Gerando os certificados
No servidor kubemaster:

mkdir -p /etc/kubernetes/certificados && cd /etc/kubernetes/certificados  

Crie um arquivo chamado openssl.cnf com o seguinte conteudo dentro de /etc/kubernetes/certificados:  

[ req ]  
distinguished_name = req_distinguished_name  
[req_distinguished_name]  
[ v3_ca ]  
basicConstraints = critical, CA:TRUE  
keyUsage = critical, digitalSignature, keyEncipherment, keyCertSign  
[ v3_req_server ]  
basicConstraints = CA:FALSE  
keyUsage = critical, digitalSignature, keyEncipherment  
extendedKeyUsage = serverAuth  
[ v3_req_client ]  
basicConstraints = CA:FALSE  
keyUsage = critical, digitalSignature, keyEncipherment  
extendedKeyUsage = clientAuth  
[ v3_req_apiserver ]  
basicConstraints = CA:FALSE  
keyUsage = critical, digitalSignature, keyEncipherment  
extendedKeyUsage = serverAuth  
subjectAltName = @alt_names_cluster  
[ v3_req_etcd ]  
basicConstraints = CA:FALSE  
keyUsage = critical, digitalSignature, keyEncipherment  
extendedKeyUsage = serverAuth, clientAuth  
subjectAltName = @alt_names_etcd  
[ alt_names_cluster ]  
DNS.1 = kubernetes  
DNS.2 = kubernetes.default  
DNS.3 = kubernetes.default.svc  
DNS.4 = kubernetes.default.svc.cluster.local  
DNS.5 = kubemaster  
IP.1 = 192.168.56.150  
IP.2 = 10.254.0.1  
[ alt_names_etcd ]  
DNS.1 = etcd1  
DNS.2 = etcd2  
IP.1 = 192.168.56.151  
IP.2 = 192.168.56.152  


CA para o kubernetes, usado para assinar os outros certificados:  

openssl ecparam -name secp521r1 -genkey -noout -out ca.key  
openssl req -x509 -new -sha256 -nodes -key ca.key -days 3650 -out ca.crt -subj "/CN=kubernetes-ca"  -extensions v3_ca -config openssl.cnf  

Certificado para o kube-apiserver:  
openssl ecparam -name secp521r1 -genkey -noout -out kube-apiserver.key  
openssl req -new -sha256 -key kube-apiserver.key -subj "/CN=kubemaster" | openssl x509 -req -sha256 -CA ca.crt -CAkey ca.key -CAcreateserial -out kube-apiserver.crt -days 3650 -extensions v3_req_apiserver -extfile openssl.cnf   


Certificado para os workers (o correto é gerar um por worker porem aqui é só um lab vai somente um para todos mesmo)  
openssl ecparam -name secp521r1 -genkey -noout -out worker.key  
openssl req -new -key worker.key -subj "/CN=worker/O=system:masters" | openssl x509 -req -sha256 -CA ca.crt -CAkey ca.key -CAcreateserial -out worker.crt -days 3650 -extensions v3_req_client -extfile openssl.cnf  

Certificado para o usuario admin administrar o cluster:  
openssl ecparam -name secp521r1 -genkey -noout -out admin.key  
openssl req -new -key admin.key -subj "/CN=kubernetes-admin/O=system:masters" | openssl x509 -req -sha256 -CA ca.crt -CAkey ca.key -CAcreateserial -out admin.crt -days 3650 -extensions v3_req_client -extfile openssl.cnf  

Certificado para service-account: (usado para assinar tokens)  
openssl ecparam -name secp521r1 -genkey -noout -out serviceaccount.key  
openssl ec -in serviceaccount.key -outform PEM -pubout -out serviceaccount.pub  
openssl req -new -sha256 -key serviceaccount.key -subj "/CN=system:kube-controller-manager" | openssl x509 -req -sha256 -CA ca.crt -CAkey ca.key -CAcreateserial -out serviceaccount.crt -days 3650 -extensions v3_req_client -extfile openssl.cnf  

Certificado para o kube-scheduler:  
openssl ecparam -name secp521r1 -genkey -noout -out kube-scheduler.key  
openssl req -new -sha256 -key kube-scheduler.key -subj "/CN=system:kube-scheduler" | openssl x509 -req -sha256 -CA ca.crt -CAkey ca.key -CAcreateserial -out kube-scheduler.crt -days 3650 -extensions v3_req_client -extfile openssl.cnf  

Certificado para o kube-proxy:  
openssl ecparam -name secp521r1 -genkey -noout -out kube-proxy.key  
openssl req -new -sha256 -key kube-proxy.key -subj "/CN=system:kube-proxy" | openssl x509 -req -sha256 -CA ca.crt -CAkey ca.key -CAcreateserial -out kube-proxy.crt -days 3650 -extensions v3_req_client -extfile openssl.cnf  

CA para assinar os certificados do etcd:  
openssl ecparam -name secp521r1 -genkey -noout -out etcd-ca.key  
openssl req -x509 -new -sha256 -nodes -key etcd-ca.key -days 3650 -out etcd-ca.crt -subj "/CN=etcd-ca" -extensions v3_ca -config openssl.cnf  

Certificado para se conectar ao etcd:  
openssl ecparam -name secp521r1 -genkey -noout -out etcd.key  
openssl req -new -sha256 -key etcd.key -subj "/CN=etcd" | openssl x509 -req -sha256 -CA etcd-ca.crt -CAkey etcd-ca.key -CAcreateserial -out etcd.crt -days 3650 -extensions v3_req_etcd -extfile openssl.cnf  

Certificado para os servidores etcd se comunicarem:  
openssl ecparam -name secp521r1 -genkey -noout -out etcd-peer.key  
openssl req -new -sha256 -key etcd-peer.key -subj "/CN=etcd-peer" | openssl x509 -req -sha256 -CA etcd-ca.crt -CAkey etcd-ca.key -CAcreateserial -out etcd-peer.crt -days 3650 -extensions v3_req_etcd -extfile openssl.cnf  

# Instalando o kubernetes

Nas maquinas kubemaster, worker1 e worker2 instale o kubernetes:  
dnf install -y kubernetes  

# Instalando o etcd

Nas maquinas etcd1 e etcd2 instale o etcd:  
dnf install -y etcd  

# Configurando os servidores etcd

Digite o comando em ambos servidores etcd:  
mkdir -p /etc/etcd/certificados  

Copie os certificados gerados na maquina kubemaster para os servidores etcd:  
scp -r etcd.crt etcd.key etcd-peer.crt etcd-peer.key etcd-ca.crt root@etcd1:/etc/etcd/certificados/  
scp -r etcd.crt etcd.key etcd-peer.crt etcd-peer.key etcd-ca.crt root@etcd2:/etc/etcd/certificados/  

Ajuste as permissoes dos arquivos nas VMs do etcd:   
chown -R etcd.etcd /etc/etcd/certificados  

Vamos editar o arquivo /etc/etcd/etcd.conf no servidor etcd1 e alterar as seguintes linhas:  
ETCD_NAME=etcd1  
ETCD_LISTEN_PEER_URLS="https://0.0.0.0:2380"  
ETCD_LISTEN_CLIENT_URLS="https://0.0.0.0:2379"  
ETCD_INITIAL_ADVERTISE_PEER_URLS="https://etcd1:2380"  
ETCD_INITIAL_CLUSTER="etcd1=https://etcd1:2380,etcd2=https://etcd2:2380"  
ETCD_ADVERTISE_CLIENT_URLS="https://0.0.0.0:2379"  
ETCD_CERT_FILE="/etc/etcd/certificados/etcd.crt"  
ETCD_KEY_FILE="/etc/etcd/certificados/etcd.key"  
ETCD_CLIENT_CERT_AUTH="true"  
ETCD_TRUSTED_CA_FILE="/etc/etcd/certificados/etcd-ca.crt"  
ETCD_PEER_CERT_FILE="/etc/etcd/certificados/etcd-peer.crt"  
ETCD_PEER_KEY_FILE="/etc/etcd/certificados/etcd-peer.key"  
ETCD_PEER_CLIENT_CERT_AUTH="true"  
ETCD_PEER_TRUSTED_CA_FILE="/etc/etcd/certificados/etcd-ca.crt"  

Vamos editar o arquivo /etc/etcd/etcd.conf no servidor etcd2 e alterar as seguintes linhas:  
ETCD_NAME=etcd2  
ETCD_LISTEN_PEER_URLS="https://0.0.0.0:2380"  
ETCD_LISTEN_CLIENT_URLS="https://0.0.0.0:2379"  
ETCD_INITIAL_ADVERTISE_PEER_URLS="https://etcd2:2380"  
ETCD_INITIAL_CLUSTER="etcd1=https://etcd1:2380,etcd2=https://etcd2:2380"  
ETCD_ADVERTISE_CLIENT_URLS="https://0.0.0.0:2379"  
ETCD_CERT_FILE="/etc/etcd/certificados/etcd.crt"  
ETCD_KEY_FILE="/etc/etcd/certificados/etcd.key"  
ETCD_CLIENT_CERT_AUTH="true"  
ETCD_TRUSTED_CA_FILE="/etc/etcd/certificados/etcd-ca.crt"  
ETCD_PEER_CERT_FILE="/etc/etcd/certificados/etcd-peer.crt"  
ETCD_PEER_KEY_FILE="/etc/etcd/certificados/etcd-peer.key"  
ETCD_PEER_CLIENT_CERT_AUTH="true"  
ETCD_PEER_TRUSTED_CA_FILE="/etc/etcd/certificados/etcd-ca.crt"  

Agora em ambos os servidores etcd:  
systemctl enable etcd && systemctl start etcd  

# Gerando os kubeconfig

Vamos gerar os .kubeconfig para os componentes do kubernetes:  
mkdir -p /etc/kubernetes/kubeconfig && cd /etc/kubernetes/kubeconfig  

# Para o controller-manager:  
kubectl config set-cluster default --certificate-authority=/etc/kubernetes/certificados/ca.crt --embed-certs=true --server=https://kubemaster:6443 --kubeconfig=controller-manager.kubeconfig  

kubectl config set-credentials system:kube-controller-manager --client-certificate=/etc/kubernetes/certificados/serviceaccount.crt --client-key=/etc/kubernetes/certificados/serviceaccount.key --embed-certs=true --kubeconfig=controller-manager.kubeconfig  

kubectl config set-context system:kube-controller-manager@default --cluster=default --user=system:kube-controller-manager --kubeconfig=controller-manager.kubeconfig   

kubectl config use-context system:kube-controller-manager@default --kubeconfig=controller-manager.kubeconfig  

# Para o kube-scheduler:  
kubectl config set-cluster default --certificate-authority=/etc/kubernetes/certificados/ca.crt --embed-certs=true --server=https://kubemaster:6443 --kubeconfig=kube-scheduler.kubeconfig  

kubectl config set-credentials system:kube-scheduler --client-certificate=/etc/kubernetes/certificados/kube-scheduler.crt --client-key=/etc/kubernetes/certificados/kube-scheduler.key --embed-certs=true --kubeconfig=kube-scheduler.kubeconfig   

kubectl config set-context system:kube-scheduler@default --cluster=default --user=system:kube-scheduler --kubeconfig=kube-scheduler.kubeconfig   

kubectl config use-context system:kube-scheduler@default --kubeconfig=kube-scheduler.kubeconfig  

# Para o usuario admin que vai gerenciar o kubernetes:  
kubectl config set-cluster default --certificate-authority=/etc/kubernetes/certificados/ca.crt --embed-certs=true --server=https://kubemaster:6443 --kubeconfig=admin.kubeconfig  

kubectl config set-credentials kubernetes-admin --client-certificate=/etc/kubernetes/certificados/admin.crt --client-key=/etc/kubernetes/certificados/admin.key --embed-certs=true --kubeconfig=admin.kubeconfig   

kubectl config set-context kubernetes-admin@default --cluster=default --user=kubernetes-admin --kubeconfig=admin.kubeconfig  

kubectl config use-context kubernetes-admin@default --kubeconfig=admin.kubeconfig  

# Para os kubelets (O correto é fazer um para cada node):  
kubectl config set-cluster default --certificate-authority=/etc/kubernetes/certificados/ca.crt --embed-certs=true --server=https://kubemaster:6443 --kubeconfig=kubelet.kubeconfig  

kubectl config set-credentials system:masters --client-certificate=/etc/kubernetes/certificados/worker.crt --client-key=/etc/kubernetes/certificados/worker.key --embed-certs=true --kubeconfig=kubelet.kubeconfig   

kubectl config set-context system:masters@default --cluster=default --user=system:masters --kubeconfig=kubelet.kubeconfig
  
kubectl config use-context system:masters@default --kubeconfig=kubelet.kubeconfig   

# Para o kubeproxy:  
kubectl config set-cluster default --certificate-authority=/etc/kubernetes/certificados/ca.crt --embed-certs=true --server=https://kubemaster:6443 --kubeconfig=kube-proxy.kubeconfig  

kubectl config set-credentials system:kube-proxy --client-certificate=/etc/kubernetes/certificados/kube-proxy.crt --client-key=/etc/kubernetes/certificados/kube-proxy.key --embed-certs=true --kubeconfig=kube-proxy.kubeconfig   

kubectl config set-context system:kube-proxy@default --cluster=default --user=system:kube-proxy --kubeconfig=kube-proxy.kubeconfig   

kubectl config use-context system:kube-proxy@default --kubeconfig=kube-proxy.kubeconfig   

Ajuste as permissoes:  
chown -R kube.kube /etc/kubernetes/kubeconfig  

# Configurando os componentes do kubernetes

No kubemaster:  

Acerte as permissoes dos certificados:  
chown -R kube.kube /etc/kubernetes/certificados/  

Alterar o arquivo /etc/kubernetes/config deixando desta maneira:  

KUBE_LOGTOSTDERR="--logtostderr=true"  
KUBE_LOG_LEVEL="--v=0"  
KUBE_MASTER="--master=https://kubemaster:6443"  
Comente a linha: KUBE_ALLOW_PRIV="--allow-privileged=false"  


Edite o arquivo /etc/kubernetes/apiserver da seguinte maneira:  

KUBE_ETCD_SERVERS="--etcd-servers=https://etcd1:2379,https://etcd2:2379"  
KUBE_SERVICE_ADDRESSES="--service-cluster-ip-range=10.254.0.0/16"  
KUBE_ADMISSION_CONTROL="--enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,ResourceQuota,PodSecurityPolicy"  
KUBE_API_ARGS="--service-account-key-file=/etc/kubernetes/certificados/serviceaccount.pub --tls-cert-file=/etc/kubernetes/certificados/kube-apiserver.crt --tls-private-key-file=/etc/kubernetes/certificados/kube-apiserver.key --kubelet-client-certificate=/etc/kubernetes/certificados/worker.crt --kubelet-client-key=/etc/kubernetes/certificados/worker.key --insecure-port=0 --client-ca-file=/etc/kubernetes/certificados/ca.crt --authorization-mode=RBAC -v=2 --advertise-address=192.168.56.150 --allow-privileged=true --proxy-client-cert-file=/etc/kubernetes/certificados/serviceaccount.crt --proxy-client-key-file=/etc/kubernetes/certificados/serviceaccount.key --etcd-cafile=/etc/kubernetes/certificados/etcd-ca.crt --etcd-certfile=/etc/kubernetes/certificados/etcd.crt --etcd-keyfile=/etc/kubernetes/certificados/etcd.key"  

Edite o arquivo /etc/kubernetes/controller-manager da seguinte forma:  

KUBE_CONTROLLER_MANAGER_ARGS="--kubeconfig=/etc/kubernetes/kubeconfig/controller-manager.kubeconfig --cluster-name=default --service-account-private-key-file=/etc/kubernetes/certificados/serviceaccount.key --cluster-signing-cert-file=/etc/kubernetes/certificados/ca.crt --cluster-signing-key-file=/etc/kubernetes/certificados/ca.key --root-ca-file=/etc/kubernetes/certificados/ca.crt --use-service-account-credentials=true --use-service-account-credentials=true --port=0 --cluster-cidr=18.254.0.0/16"  

Edite o arquivo /etc/kubernetes/scheduler da seguinte forma:  

KUBE_SCHEDULER_ARGS="--kubeconfig=/etc/kubernetes/kubeconfig/kube-scheduler.kubeconfig --port=0"  

Edite o arquivo /etc/kubernetes/proxy da seguinte forma:  

KUBE_PROXY_ARGS="--kubeconfig=/etc/kubernetes/kubeconfig/kube-proxy.kubeconfig --cluster-cidr=18.254.0.0/16"  


Edite o arquivo /etc/kubernetes/kubelet da seguinte forma:  
KUBELET_ADDRESS="--address=0.0.0.0"  
KUBELET_PORT="--port=10250"  
KUBELET_HOSTNAME="--hostname-override=kubemaster"  
KUBELET_ARGS="--cgroup-driver=systemd --fail-swap-on=false --kubeconfig=/etc/kubernetes/kubeconfig/kubelet.kubeconfig --cluster-dns=10.254.0.10 --network-plugin=cni"  

Ative e inicie os serviços:  

systemctl enable kube-apiserver kube-controller-manager kube-scheduler kube-proxy kubelet  
systemctl start kube-apiserver kube-controller-manager kube-scheduler kube-proxy kubelet  

Copiar o arquivo kubeconfig do usuario para a pasta /home/usuario/.kube/config  
cp -r /etc/kubernetes/kubeconfig/admin.kubeconfig /root/.kube/config

Copie o certificado e os kubeconfigs para o /etc/kubernetes/ do worker1:  
scp -r /etc/kubernetes/certificados/worker* root@worker1:/etc/kubernetes  
scp -r /etc/kubernetes/kubeconfig/kubelet.kubeconfig root@worker1:/etc/kubernetes  
scp -r /etc/kubernetes/kubeconfig/kube-proxy.kubeconfig root@worker1:/etc/kubernetes  

Copie o certificado e o kubeconfig para o /etc/kubernete/s do worker2:  
scp -r /etc/kubernetes/certificados/worker* root@worker2:/etc/kubernetes  
scp -r /etc/kubernetes/kubeconfig/kubelet.kubeconfig root@worker2:/etc/kubernetes  
scp -r /etc/kubernetes/kubeconfig/kube-proxy.kubeconfig root@worker2:/etc/kubernetes  

# Configurando o worker1  
No Worker1:  
chown kube.kube /etc/kubernetes/worker*  
chown kube.kube /etc/kubernetes/kubelet.kubeconfig  
chown kube.kube /etc/kubernetes/kube-proxy.kubeconfig  


Edite o /etc/kubernetes/kubelet do worker1 da seguinte forma:  

KUBELET_ADDRESS="--address=0.0.0.0"  
KUBELET_HOSTNAME="--hostname-override=worker1"  
KUBELET_ARGS="--cgroup-driver=systemd --kubeconfig=/etc/kubernetes/kubelet.kubeconfig --cluster-dns=10.254.0.10 --network-plugin=cni --cluster-domain=cluster.local"  


Edite o arquivo /etc/kubernetes/config da seguinte forma:  
KUBE_LOGTOSTDERR="--logtostderr=true"  
KUBE_LOG_LEVEL="--v=0"  
KUBE_MASTER="--master=https://kubemaster:6443"  
Comente a linha: KUBE_ALLOW_PRIV="--allow-privileged=false"  


Edite o arquivo /etc/kubernetes/proxy da seguinte maneira:  
KUBE_PROXY_ARGS="--kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig --cluster-cidr=18.254.0.0/16"  

Ative e inicie os serviços:  

systemctl enable kubelet kube-proxy && systemctl start kubelet kube-proxy  

# Configurando o worker2:  
No Worker2:  

chown kube.kube /etc/kubernetes/worker*  
chown kube.kube /etc/kubernetes/kubelet.kubeconfig  
chown kube.kube /etc/kubernetes/kube-proxy.kubeconfig


Edite o arquivo /etc/kubernetes/kubelet da seguinte forma:  

KUBELET_ADDRESS="--address=0.0.0.0"  
KUBELET_HOSTNAME="--hostname-override=worker2"  
KUBELET_ARGS="--cgroup-driver=systemd --kubeconfig=/etc/kubernetes/kubelet.kubeconfig --cluster-dns=10.254.0.10 --network-plugin=cni --cluster-domain=cluster.local"


Edite o arquivo /etc/kubernetes/config da seguinte forma:  
KUBE_LOGTOSTDERR="--logtostderr=true"  
KUBE_LOG_LEVEL="--v=0"  
KUBE_MASTER="--master=https://kubemaster:6443"  
Comente a linha: KUBE_ALLOW_PRIV="--allow-privileged=false"  

Edite o arquivo /etc/kubernetes/proxy da seguinte maneira:  
KUBE_PROXY_ARGS="--kubeconfig=/etc/kubernetes/kube-proxy.kubeconfig --cluster-cidr=18.254.0.0/16"  

Ative e inicie os serviços:  

systemctl enable kubelet kube-proxy && systemctl start kubelet kube-proxy  

# Configurando calico, coredns e metricas:  

Voltando ao kubemaster:  
wget https://gitlab.com/jpmarcucci/kubernetes-my-way/-/archive/master/kubernetes-my-way-master.zip  
unzip master.zip  

Aplique as configuracoes do Pod Security Policy:  
kubectl apply -f kubernetes-my-way-master/yaml/infra/rbac/podsecurity.yaml  
kubectl apply -f kubernetes-my-way-master/yaml/infra/rbac/calico.yaml   

Deploy do calico:  
Editar o arquivo calico-etcd.yaml colocar os certificados do etcd, alterar os IPs dos servidores etcd e alterar o IP do pool.  
kubectl apply -f kubernetes-my-way-master/yaml/infra/calico-etcd.yaml   

Deploy do coredns:  
kubectl apply -f kubernetes-my-way-master/yaml/infra/coredns.yaml  

Metrics-server:  
kubectl apply -f kubernetes-my-way-master/yaml/infra/metrics-server  

Metallb:  
kubectl apply -f kubernetes-my-way-master/yaml/infra/metallb.yaml  
kubectl apply -f kubernetes-my-way-master/yaml/infra/metallb-configuration.yaml  


# Esse guia foi baseado nos seguintes tutoriais:  

https://kubernetes.io/docs/getting-started-guides/fedora/fedora_manual_config/  
https://kubernetes.io/docs/getting-started-guides/fedora/flannel_multi_node_cluster/  
https://github.com/kelseyhightower/kubernetes-the-hard-way/  
https://nixaid.com/deploying-kubernetes-cluster-from-scratch/  
https://docs.projectcalico.org/v3.8/introduction/  
https://github.com/kubernetes-incubator/metrics-server  
https://metallb.universe.tf/  
